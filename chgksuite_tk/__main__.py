#!/usr/bin/env python
# -*- coding: utf-8 -*-
from chgksuite_tk.gui import app


def main():
    app()


if __name__ == "__main__":
    main()
